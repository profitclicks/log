package log

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

const timeFormat = "2006.01.02 15:04:05"

// EnvLevel holds the name of environment variable for the log level.
// It's here for the convenience.
const EnvLevel = "LOG_LEVEL"

// DefaultLevel represents the default log level.
const DefaultLevel = Info

// The list of available log levels.
const (
	Info Level = iota
	Warning
	Error
	Critical
)

var known = map[Level]bool{
	Info:     true,
	Warning:  true,
	Error:    true,
	Critical: true,
}

var levelString = map[Level]string{
	Info:     "<Info>",
	Warning:  "<Warning>",
	Error:    "<Error>",
	Critical: "<Critical>",
}

var stringLevel = map[string]Level{
	"info":     Info,
	"warn":     Warning,
	"warning":  Warning,
	"error":    Error,
	"critical": Critical,
}

var bufPool = sync.Pool{
	New: func() interface{} {
		return new(bytes.Buffer)
	},
}

// Level defines the log Level.
type Level uint32

// String returns a string representation of log level.
func (l Level) String() string {
	switch l {
	case Info:
		return "info"
	case Warning:
		return "warning"
	case Error:
		return "error"
	case Critical:
		return "critical"
	default:
		return "invalid"
	}
}

// Logger defines the properties of the logger.
type Logger struct {
	mu    sync.Mutex
	out   io.Writer
	level Level
}

// New creates a new Logger with the output assigned to stderr and the default log level.
func New() *Logger {
	return &Logger{out: os.Stderr, level: DefaultLevel}
}

// ParseLevel parses the level string.
// If the name of level is unknown it returns an error and the default level.
func ParseLevel(s string) (Level, error) {
	l, ok := stringLevel[strings.ToLower(strings.TrimSpace(s))]
	if !ok {
		return DefaultLevel, fmt.Errorf("unknown log level: %v", s)
	}
	return l, nil
}

// SetOutput sets the output destination for the logger.
func (l *Logger) SetOutput(w io.Writer) {
	l.mu.Lock()
	l.out = w
	l.mu.Unlock()
}

// SetLevel changes the log level to the provided.
func (l *Logger) SetLevel(level Level) error {
	if level == l.level {
		return nil
	}
	if !known[level] {
		return fmt.Errorf("unknown log level: %v", level)
	}
	atomic.StoreUint32((*uint32)(&l.level), uint32(level))
	return nil
}

// Level returns the current value of the log level.
func (l *Logger) Level() Level {
	return l.level
}

// Output writes the output for a logging event.
func (l *Logger) Output(level Level, s string) error {
	now := time.Now().Format(timeFormat)

	b := bufPool.Get().(*bytes.Buffer)
	b.Reset()

	b.WriteString(now)
	b.WriteByte(' ')
	b.WriteString(levelString[level])
	b.WriteByte(' ')
	b.WriteString(s)
	if len(s) == 0 || s[len(s)-1] != '\n' {
		b.WriteByte('\n')
	}

	_, err := l.out.Write(b.Bytes())

	bufPool.Put(b)

	return err
}

// Info prints to the logger at "Info" log level.
func (l *Logger) Info(v ...interface{}) {
	if l.level > Info {
		return
	}
	l.Output(Info, fmt.Sprint(v...))
}

// Infof prints formatted output to the logger at "Info" log level.
func (l *Logger) Infof(format string, v ...interface{}) {
	if l.level > Info {
		return
	}
	l.Output(Info, fmt.Sprintf(format, v...))
}

// Warn prints to the logger at "Warning" log level.
func (l *Logger) Warn(v ...interface{}) {
	if l.level > Warning {
		return
	}
	l.Output(Warning, fmt.Sprint(v...))
}

// Warnf prints formatted output to the logger at "Warning" log level.
func (l *Logger) Warnf(format string, v ...interface{}) {
	if l.level > Warning {
		return
	}
	l.Output(Warning, fmt.Sprintf(format, v...))
}

// Error prints to the logger at "Error" log level.
func (l *Logger) Error(v ...interface{}) {
	if l.level > Error {
		return
	}
	l.Output(Error, fmt.Sprint(v...))
}

// Errorf prints formatted output to the logger at "Error" log level.
func (l *Logger) Errorf(format string, v ...interface{}) {
	if l.level > Error {
		return
	}
	l.Output(Error, fmt.Sprintf(format, v...))
}

// Critical prints to the logger at "Critical" log level.
func (l *Logger) Critical(v ...interface{}) {
	if l.level > Critical {
		return
	}
	l.Output(Critical, fmt.Sprint(v...))
}

// Criticalf prints formatted output to the logger at "Critical" log level.
func (l *Logger) Criticalf(format string, v ...interface{}) {
	if l.level > Critical {
		return
	}
	l.Output(Critical, fmt.Sprintf(format, v...))
}

// Fatal prints to the logger at "Critical" log level and finishes the app.
func (l *Logger) Fatal(v ...interface{}) {
	l.Output(Critical, fmt.Sprint(v...))
	os.Exit(1)
}

// Fatalf prints formatted output to the logger at "Critical" log level and finishes the app.
func (l *Logger) Fatalf(format string, v ...interface{}) {
	l.Output(Critical, fmt.Sprintf(format, v...))
	os.Exit(1)
}

// Panic prints to the logger at "Critical" log level and panics.
func (l *Logger) Panic(v ...interface{}) {
	s := fmt.Sprint(v...)
	l.Output(Critical, s)
	panic(s)
}

// Panicf prints formatted output to the logger at "Critical" log level and panics.
func (l *Logger) Panicf(format string, v ...interface{}) {
	s := fmt.Sprintf(format, v...)
	l.Output(Critical, s)
	panic(s)
}

// Print prints to the logger at "Info" log level.
// It is added for backward compatibility with the standard logger.
func (l *Logger) Print(v ...interface{}) {
	if l.level > Info {
		return
	}
	l.Output(Info, fmt.Sprint(v...))
}

// Printf prints formatted output to the logger at "Info" log level.
// It is added for backward compatibility with the standard logger.
func (l *Logger) Printf(format string, v ...interface{}) {
	if l.level > Info {
		return
	}
	l.Output(Info, fmt.Sprintf(format, v...))
}
